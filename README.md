# HI-CAM EDK

Collected sources for down-scaling data for HI-CAM with EDK.

## PDF files

You can browse and download the generated PDF files [here](https://muellese.pages.ufz.de/hi-cam-edk).

All PDF files can be downloaded as `.zip` file [here](https://git.ufz.de/muellese/hi-cam-edk/-/jobs/artifacts/master/download?job=pdf&file_type=archive).
