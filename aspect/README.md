# Corrected aspect drift generation

## Subject

- we assume radiation to be correlated to a modified aspect derived from the DEM
- radiation at south facing areas should be 40% more than north facing ones
- by default, we use shape 3 of the following left image:

  ![Drift](edk_corr_aspect.png)

- the corrected aspect drift will be calculated at the input- and output-resolution
- user needs to provide DEM at both resolutions in separate file

## Script

### Conda

To prepare your conda environment, install the following packages:

```bash
conda install -y -c conda-forge xarray netcdf4 xarray-spatial
```

### Usage

```bash
python dem_to_aspect.py [-h] [-i DEM_IN] [-o DEM_OUT]

DEM to corrected aspect drift conversion.

optional arguments:
  -h, --help  show this help message and exit
  -i DEM_IN   In DEM
  -o DEM_OUT  Out DEM
```
