"""DEM to corrected aspect drift conversion."""
import os
import copy
import argparse
import numpy as np
import scipy.interpolate as spi
import xarray as xr
import xrspatial as xrs


# Default values.
IN_DEM = "dem_in.nc"
OUT_DEM = "dem_hicam_3km_eu.nc"


def parse_args():
    """Parse the given arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__,
    )
    parser.add_argument(
        "-i", action="store", default=IN_DEM, dest="dem_in", help="In DEM",
    )
    parser.add_argument(
        "-o", action="store", default=OUT_DEM, dest="dem_out", help="Out DEM",
    )
    args = parser.parse_args()
    raw_paths = args.dem_in, args.dem_out
    out_paths = []
    cwd, here = os.getcwd(), os.path.dirname(os.path.realpath(__file__))
    for path in raw_paths:
        if not os.path.isabs(path):
            cwd_p = os.path.join(cwd, path)
            here_p = os.path.join(here, path)
            path = cwd_p if os.path.exists(cwd_p) else here_p
        out_paths.append(path)
    return out_paths


def msh(coords, names):
    """Generate mesh from wanted coords."""
    # check if we have "dimension coordinates" (they have themself as dim)
    gen_mesh = all([name in coords[name].dims for name in names])
    # get arrays
    pos = [coords[name].data for name in names]
    return np.meshgrid(*pos, indexing="ij") if gen_mesh else pos


def inter_nearest(arr, in_names=None, coords=None, out_names=None, mask=False):
    """
    Interpolate with nearest value.

    Parameters
    ----------
    arr : DataArray
        Input data.
    in_names : tuple of strings, optional
        Names of input_coords to use. The default is 'arr.dims'
    coords : xarray coordinates, optional
        Coordinates to use for output. The default is arr.coords.
    out_names : tuple of strings, optional
        Names of output_coords to use. The default is 'in_names'
    mask : array, optional
        mask of resulting array. To be filled with nan. The default is False.

    Returns
    -------
    array
        Output data.
    """
    # input position tuple
    in_names = arr.dims if in_names is None else in_names
    pos_in = msh(arr.coords, in_names)
    # output position tuple
    coords = arr.coords if coords is None else coords
    out_names = copy.copy(in_names) if out_names is None else out_names
    pos_out = msh(coords, out_names)
    # nan value masking
    non_nan = ~np.isnan(arr.data)
    pos = np.array([p[non_nan] for p in pos_in]).T
    val = arr.data[non_nan]
    # generate output
    interp = spi.NearestNDInterpolator(pos, val)
    out = interp(*pos_out)
    out[mask] = np.nan
    return out


def trans(x, x0=0, x1=1, y0=0, y1=1, s=1):
    """Transition function from (x0,y0) to (x1,y1) with shape control."""
    x = np.array(x, ndmin=1, dtype=float)
    y = np.full_like(x, y0)
    y[x >= x1] = y1
    xt = np.logical_and(x0 < x, x < x1)
    x_val = (x[xt] - x0) / (x1 - x0) * 2 - 1
    y[xt] = y0 + (np.tanh(s * np.arctanh(x_val)) / 2 + 0.5) * (y1 - y0)
    return y


def get_drift(x, north=1, south=1.4, angle_tol=0, shape=3):
    """Correction function for aspect drift.

    Parameters
    ----------
    x : array-like
        aspect.
    north : float, optional
        north correction value. The default is 1.
    south : float, optional
        south correction value. The default is 1.4.
    angle_tol : float, optional
        angular tolerance around north and south. The default is 0.
    shape : float, optional
        shape value for transition from north to south. The default is 3.

    Returns
    -------
    y : numpy.ndarray
        corrected aspect
    """
    x = np.array(x, dtype=float, ndmin=1)
    y = np.full_like(x, north)
    y[np.abs(x - 180) <= angle_tol] = south
    n0, n1 = 360 - angle_tol, angle_tol
    s0, s1 = 180 - angle_tol, 180 + angle_tol
    n2s = np.logical_and(x > n1, x < s0)
    s2n = np.logical_and(x > s1, x < n0)
    y[n2s] = trans(x[n2s], n1, s0, north, south, s=shape)
    y[s2n] = trans(x[s2n], s1, n0, south, north, s=shape)
    min_y, max_y = min(north, south), max(north, south)
    return np.minimum(np.maximum(y, min_y), max_y)


if __name__ == "__main__":
    dem_in, dem_out = parse_args()
    asp_in = os.path.splitext(dem_in)[0] + "_aspect.nc"
    asp_out = os.path.splitext(dem_out)[0] + "_aspect.nc"
    # open IN- and OUT-put DEM
    data_in = xr.open_dataset(dem_in)
    data_out = xr.open_dataset(dem_out)
    # generate aspect (2D array given by lat-lon)
    aspect = xrs.aspect(data_out["dem"], "aspect_drift")
    # aspect is -1 where it couldn't be determined -> set to north
    aspect.data[aspect.data < 0] = 180
    # correct data (shift of 180 degree, since xr-spatial sets south to 0)
    select_aspect = ~np.isnan(aspect.data)
    aspect.data[select_aspect] = (aspect.data[select_aspect] + 180) % 360
    data_out["aspect_drift"] = aspect
    data_out["aspect_drift"].data = get_drift(inter_nearest(aspect))
    # generate DEM masks
    mask_dem_in = np.isnan(data_in["dem"].data)
    mask_dem_out = np.isnan(data_out["dem"].data)
    aspect_in = inter_nearest(
        arr=data_out["aspect_drift"],
        coords=data_in["dem"].coords,
        out_names=["lat", "lon"],
        mask=mask_dem_in,
    )
    # save IN
    data_in["aspect_drift"] = data_in["dem"].copy(data=aspect_in)
    print(data_in["aspect_drift"])
    data_in["aspect_drift"].attrs['_FillValue'] = -9999
    data_in["aspect_drift"].attrs['missing_value'] = -9999
    data_in.to_netcdf(asp_in)
    # save OUT
    data_out["aspect_drift"].data[mask_dem_out] = np.nan
    data_out["aspect_drift"].attrs['_FillValue'] = -9999
    data_out["aspect_drift"].attrs['missing_value'] = -9999
    data_out.to_netcdf(asp_out)
    # final check
    mask_aspect_in = np.isnan(data_in["aspect_drift"].data)
    mask_aspect_out = np.isnan(data_out["aspect_drift"].data)
    print("Missing Points IN :", np.sum(mask_dem_in != mask_aspect_in))
    print("Missing Points OUT:", np.sum(mask_dem_out != mask_aspect_out))
