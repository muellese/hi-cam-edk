# Specific Humidity (huss)

Should be interpolated as relative humidity, since the literature provides variograms only for this variable

## Workflow
1. compute relative humidity from huss on input resolution
2. disaggregate relative humidity with EDK (dem drift)
3. re-compute huss from relative humidity on output resolution

### Notation:
- temperature (temp): $`T`$ in $`[^{\circ}C]`$
- surface pressure (ps): $`p`$ in $`[Pa]`$
- specific humidity (huss): $`q`$ in $`[\frac{kg}{kg}]`$
- saturated specific humidity: $`q_s`$ in $`[\frac{kg}{kg}]`$
- saturated vapor pressure: $`e_s`$ in $`[mb]`$
- enhanced saturated vapor pressure: $`e_e`$ in $`[mb]`$
- **relative humidity (RH)**: $`h_r`$

### Formulas
We are following

>  Buck, A. L. (1981). New Equations for Computing Vapor Pressure and Enhancement Factor,
> Journal of Applied Meteorology and Climatology, 20(12), 1527-1532.
> Retrieved Mar 29, 2021, from https://journals.ametsoc.org/view/journals/apme/20/12/1520-0450_1981_020_1527_nefcvp_2_0_co_2.xml

We use the following formula for relative humidity:
```math
\begin{aligned}
h_r &= \frac{q}{q_s}\\
q_s &= \frac{c_1 \cdot e_e}{\frac{p}{100} - c_2 \cdot e_e} \\
e_e &= (1 + X + \frac{p}{100} \cdot (Y + Z \cdot T^2)) \cdot e_s \\
e_s &= A \cdot \exp \left(\left(B - \frac{T}{D}\right) \cdot \frac{T}{C + T} \right)
\end{aligned}
```

And the following formula for specific humidity:
```math
\begin{aligned}
q   &= h_r \cdot q_s
\end{aligned}
```

With constants:
```math
\begin{aligned}
c_1 &= 0.62198~[-]\\
c_2 &= 0.37802~[-]\\
A &= 6.1115~(T\leq 0)~6.1121~(T > 0)~[mb]\\
B &= 23.036~(T\leq 0)~18.729~(T > 0)~[-]\\
C &= 279.82~(T\leq 0)~257.87~(T > 0)~[K]\\
D &= 333.7~(T\leq 0)~227.3~(T > 0)~[°C]\\
X &= 2.2e-4~(T\leq 0)~7.2e-4~(T > 0)~[-]\\
Y &= 3.83e-6~(T\leq 0)~3.2e-6~(T > 0)~[mb^{-1}]\\
Z &= 6.4e-10~(T\leq 0)~5.9e-10~(T > 0)~[mb^{-1} °C^{-2}]
\end{aligned}
```

## input

- input grid
    - dem
    - temperature (temp)
    - surface pressure (ps)
    - specific humidity (huss)

- output grid
    - dem
    - temperature (temp)
    - surface pressure (ps)

- pre-processor
    - calculate relative humidity on input gird

## output

- output grid
    - relative humidity

- post-processor
    - calculate specific humidity on output gird

# Script
Convert specific humidity to relative humidity and vice versa.

Inputs
------
- specific humidity / relative humidity
- surface pressure
- temperature

Outputs
-------
- relative humidity / specific humidity

Default File Names
------------------
- huss[_in/_out].nc
- rh[_in/_out].nc
- ps[_in/_out].nc
- tas[_in/_out].nc

[_in/_out] suffix is depending on the "-i" flag.
If "-i" is present "_out" will be used, otherwise "_in".

Conda
-----
To prepare your conda environment, install the following packages:

```bash
conda install -y -c conda-forge xarray netcdf4
```
for huss_to_rh_timeslice.py additionally the netcdf4 wrapper from the ufz python package is needed.

```bash
pip install git@git.ufz.de:chs/python.git
```
usage
-----
The script has the following calling signature

```bash
python huss_to_rh.py [-h] [-i] [-q HUSS_F] [-r RH_F] [-p PS_F] [-t TAS_F] [-c]

optional arguments:
    -h, --help     show this help message and exit
    -i, --inverse  If present, calculate spec. humidity from relative humidity.
    -q HUSS_F      nc-file with specific humidity ('huss' as var)
    -r RH_F        nc-file with relative humidity ('rh' as var)
    -p PS_F        nc-file with surface pressure ('ps' as var [Pa])
    -t TAS_F       nc-file with avg. temp. ('tas[Adjust]' as var [K])
    -c, --cut      If present, rel. humidity is forced to be in [0,1].
```

The script huss_to_rh_timeslice.py allows to process and write the data per timeslice 
(to avoid memory issues with larger datasets).


Examples
--------
Generate `rh.nc`:

```bash
python huss_to_rh.py -q huss.nc -r rh.nc -p pa.nc -t tas.nc
```

Generate `huss.nc`:

```bash
python huss_to_rh.py -i -q huss.nc -r rh.nc -p pa.nc -t tas.nc
```
