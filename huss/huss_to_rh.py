"""
Convert specific humidity to relative humidity and vice versa.

Inputs
------
- specific humidity / relative humidity
- surface pressure
- temperature

Outputs
-------
- relative humidity / specific humidity

Default File Names
------------------
- huss[_in/_out].nc
- rh[_in/_out].nc
- ps[_in/_out].nc
- tas[_in/_out].nc

[_in/_out] suffix is depending on the "-i" flag.
If "-i" is present "_out" will be used, otherwise "_in".

Examples
--------
Generate `rh.nc`:

    python huss_to_rh.py -q huss.nc -r rh.nc -p pa.nc -t tas.nc

Generate `huss.nc`:

    python huss_to_rh.py -i -q huss.nc -r rh.nc -p pa.nc -t tas.nc
"""
import os
import argparse
import numpy as np
import xarray as xr

# para [t <= 0, t > 0]
# Buck 1981: ei3, ew4
A = [6.1115, 6.1121]
B = [23.036, 18.729]
C = [279.82, 257.87]
D = [333.7, 227.3]
# Buck 1981: fi4, fw4
X = [2.2e-4, 7.2e-4]
Y = [3.83e-6, 3.2e-6]
Z = [6.4e-10, 5.9e-10]
V = [0, 0]
W = [0, 0]
# Buck 1981: fi5, fw5
# X = [4.8e-4, 4.1e-4]
# Y = [3.47e-6, 3.48e-6]
# Z = [5.9e-10, 7.4e-10]
# V = [23.8, 30.6]
# W = [-3.1e-2, -3.8e-2]

# constants for vapor pressure [e]
C1 = 0.62198
C2 = 0.37802

# Absolut zero
T0 = 273.15  # K


def parse_args():
    """Parse the given arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__,
    )
    parser.add_argument(
        "-i",
        "--inverse",
        action="store_true",
        default=False,
        dest="inv",
        help="If present, calculate spec. humidity from relative humidity.",
    )
    parser.add_argument(
        "-q",
        action="store",
        default="huss",
        dest="huss_f",
        help="nc-file with specific humidity ('huss' as var)",
    )
    parser.add_argument(
        "-r",
        action="store",
        default="rh",
        dest="rh_f",
        help="nc-file with relative humidity ('rh' as var)",
    )
    parser.add_argument(
        "-p",
        action="store",
        default="ps",
        dest="ps_f",
        help="nc-file with surface pressure ('ps' as var [Pa])",
    )
    parser.add_argument(
        "-t",
        action="store",
        default="tas",
        dest="tas_f",
        help="nc-file with avg. temp. ('tas[Adjust]' as var [K])",
    )
    parser.add_argument(
        "-c",
        "--cut",
        action="store_true",
        default=False,
        dest="cut",
        help="If present, rel. humidity is forced to be in [0,1].",
    )
    args = parser.parse_args()
    raw_paths = args.huss_f, args.ps_f, args.tas_f, args.rh_f
    inv = args.inv
    raw_paths = [
        f if f.endswith(".nc") else f + ("_out" if inv else "_in") + ".nc"
        for f in raw_paths
    ]
    # here = os.path.dirname(os.path.realpath(__file__))
    cwd = os.getcwd()
    out = []
    for path in raw_paths:
        out.append(path if os.path.isabs(path) else os.path.join(cwd, path))
    return out + [args.cut, args.inv]


def enh_sat_vap(p, t):
    """
    Enhanced Saturation Vapor Pressure.

    Parameters
    ----------
    p : array_like
        surface pressure in [Pa].
    t : array_like
        temperature in [K].

    Returns
    -------
    array_like
        Enhanced Saturation Vapor Pressure.
    """
    t = np.array(t)
    p = np.array(p)
    t -= T0  # convert K to deg C
    out = np.full_like(t, np.nan)
    tm = t <= 0
    tp = t > 0
    tl0 = t[tm]  # less-equal 0
    tg0 = t[tp]  # greater 0
    pl0 = 0.01 * p[tm]
    pg0 = 0.01 * p[tp]
    out[tm] = A[0] * np.exp((B[0] - tl0 / D[0]) * tl0 / (tl0 + C[0]))
    out[tp] = A[1] * np.exp((B[1] - tg0 / D[1]) * tg0 / (tg0 + C[1]))
    # enhancement factor
    out[tm] *= 1 + X[0] + pl0 * (Y[0] + Z[0] * (tl0 + V[0] + W[0] * pl0) ** 2)
    out[tp] *= 1 + X[1] + pg0 * (Y[1] + Z[1] * (tg0 + V[1] + W[1] * pg0) ** 2)
    return out


def sat_spec_hum(p, t):
    """
    Saturated specific humidity.

    Parameters
    ----------
    p : array_like
        surface pressure in [Pa].
    t : array_like
        temperature in [K].

    Returns
    -------
    array_like
        Saturated specific humidity.
    """
    enh_sat = enh_sat_vap(p, t)
    return C1 * enh_sat / (0.01 * p - C2 * enh_sat)


def rel_hum(q, p, t):
    """
    Relative humidity.

    Parameters
    ----------
    q : array_like
        specific humidity.
    p : array_like
        surface pressure in [Pa].
    t : array_like
        temperature in [K].

    Returns
    -------
    array_like
        relative humidity.
    """
    return q / sat_spec_hum(p, t)


def spec_hum(r, p, t):
    """
    Specific humidity.

    Parameters
    ----------
    r : array_like
        relative humidity.
    p : array_like
        surface pressure in [Pa].
    t : array_like
        temperature in [K].

    Returns
    -------
    array_like
        Specific humidity.
    """
    return r * sat_spec_hum(p, t)


if __name__ == "__main__":
    huss_f, ps_f, tas_f, rh_f, cut, inv = parse_args()
    # input output file names
    in_f = rh_f if inv else huss_f
    out_f = huss_f if inv else rh_f
    # input output variable names
    in_n = "rh" if inv else "huss"
    out_n = "huss" if inv else "rh"
    # open IN- and OUT-put
    print("Reading input data...")
    print("", in_f)
    print("", ps_f)
    print("", tas_f)
    data_in = xr.open_dataset(in_f)
    data_p = xr.open_dataset(ps_f)
    data_t = xr.open_dataset(tas_f)
    data_out = data_in.copy(deep=True)
    # print(data_in[in_n].attrs)
    name_t = "tas" if "tas" in data_t else "tasAdjust"
    name_p = "ps" if "ps" in data_p else "psAdjust"
    # print(data_t[name_t].attrs)
    print("Converting data...")
    print(" Using '{}' for temp.".format(name_t))
    if inv:
        attrs = {
            "standard_name": "specific_humidity",
            "long_name": "Near-Surface Specific Humidity",
            "units": "1",
            "comment": "near-surface (usually, 2 meter) specific humidity.",
        }
        out = spec_hum(data_in[in_n], data_p[name_p], data_t[name_t])
        if cut:
            print("cut values < 0 for specific humidity!")
            out = np.maximum(out,0)
    else:
        attrs = {
            "standard_name": "relative_humidity",
            "long_name": "Relative Humidity",
            "units": "1",
        }
        out = rel_hum(data_in[in_n],  data_p[name_p], data_t[name_t])

        if cut:
            print("cut values < 0 and > 1 for relative humidity!")
            out = np.maximum(np.minimum(out, 1), 0)
 
    print("Writing output...")
    print("", out_f)
    data_out[out_n] = xr.DataArray(
        out,
        name=out_n,
        coords=data_in[in_n].coords,
        dims=data_in[in_n].dims,
        attrs=attrs,
    )
    del data_out[in_n]
    data_out.to_netcdf(out_f)
